import React, {useState, useEffect} from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import "../Style/styles.css"
import Agenda from './AgendaComponent';
import CommitmentView from './CommitmentView'
import api from '../service/api'

const Routes = () => {
  const [commitments, setCommitments] = useState([])
  useEffect (() => {
    api.get("/commitments")
    .then(resp => {
      setCommitments(resp.data)      
    })
  }, [])
  return (
    <Router>
      <header className="header"></header>

      <Switch>
        <Route path = "/agenda">
          <Agenda commitments={commitments}/>
        </Route>
        <Route path = {`/compromissos/`}>
          <CommitmentView commitments={commitments}/>
        </Route>
      </Switch>


    </Router>
  )
}

export default Routes 