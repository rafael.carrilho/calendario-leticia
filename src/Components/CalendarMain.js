import React, { useState, useEffect } from 'react'
import CalendarDays from './CalendarDays'
import '../Style/styles.css'
import {Link} from 'react-router-dom'

const CalendarMain = props => {
  let dateNow = new Date()
  var meses = [
    "Janeiro",
    "Fevereiro",
    "Março",
    "Abril",
    "Maio",
    "Junho",
    "Julho",
    "Agosto",
    "Setembro",
    "Outubro",
    "Novembro",
    "Dezembro"
  ];
  
  const [month, setMonth] = useState(dateNow.getMonth())
  const [year, setYear] = useState(dateNow.getFullYear())
  const [days, setDays] = useState([])
  const add_zero = (number) => {
    if (number < 10){
      return "0"+number
    }
    return number
  }
  const changeMonth = number => {
    setDays([])
    if (month === 11 && number === 1) {
      setMonth(0)
      setYear(year+1)
      let firstDay = new Date(year+1, 0, 1)
      let lastDay = new Date(year+1, 1, 0)
      let lastMonthDay = new Date(year, 0, 0)
      handlerDaysChange(firstDay, lastDay, lastMonthDay)

    }else if (month === 0 && number === -1){
      setYear(year-1)
      setMonth(11)
      let firstDay = new Date(year-1, 11, 1)
      let lastMonthDay = new Date(year, 11, 0)
      let lastDay = new Date(year-1, 0, 0)
      handlerDaysChange(firstDay, lastDay, lastMonthDay)
    }else{
      setMonth(month + number)
      let firstDay = new Date(year, month + number, 1)
      let lastDay = new Date(year, month + 1 + number, 0)
      let lastMonthDay = new Date(year, month + number, 0)
      handlerDaysChange(firstDay, lastDay, lastMonthDay)
    }
  }
  const handlerDaysChange = (firstDay, lastDay, lastMonthDay) => {
    let aux = firstDay.getDate() + lastDay.getDate()
    let newDaysAll=[]
    for (let i = 0; i < firstDay.getDay(); i++) {
      let dia= add_zero(lastMonthDay.getDate() - firstDay.getDay() + i + 1);
      let mes= add_zero(firstDay.getMonth());
      let ano= add_zero(lastMonthDay.getFullYear());
      let this_date = dia + "/" + mes + '/' + ano ;
      let classNameLink = checkHasCommit(this_date)
      newDaysAll.push (
        <Link to={`compromissos/${this_date}`} key={this_date} className={`${classNameLink} clickableDiv`}>
            <p className="dayOff">{dia}</p>
        </Link>        
      )
    }
    for (let k = 1; k < aux; k++){
      let dia= add_zero(k);
      let mes= add_zero(firstDay.getMonth()+1);
      let ano= add_zero(firstDay.getFullYear());
      let this_date = dia + "/" + mes + '/' + ano ;
      let classNameLink = checkHasCommit(this_date)
      newDaysAll.push (
        <Link to={`compromissos/${this_date}`} key={this_date} className={`${classNameLink} clickableDiv`}> 
          <p id ={this_date} className="dayIn"> {dia} </p>
        </Link>
      )
    }
    let auxday = 1

    for (let w = lastDay.getDay(); w < 6; w++) {
      let dia= add_zero(auxday);
      let mes= add_zero((firstDay.getMonth()+1)>11 ? 0 : firstDay.getMonth()+ 2);
      let ano= add_zero((firstDay.getMonth()+1)>11 ? year + 1 : year);
      let this_date = dia + "/" + mes + '/' + ano ;  
      let classNameLink = checkHasCommit(this_date)
      newDaysAll.push (
        <Link to={`compromissos/${this_date}`} key={this_date} className={`${classNameLink} clickableDiv`}> 
          <p id ={this_date} className="dayOff"> {auxday} </p>
        </Link>
        )
      auxday ++
    }
    setDays(newDaysAll)
  }
  useEffect(()=> {
    let firstDay = new Date(year, month, 1)
    let lastDay = new Date(year, month+1, 0)
    let lastMonthDay = new Date(year, month, 0)
    handlerDaysChange(firstDay, lastDay, lastMonthDay)
  }, [])

  
  
  const checkHasCommit = datefull => {
    let return_txt = ''
    props.commitments.forEach(e => {
      if (e.day_of == datefull){
        return_txt = "has_commitment"
      }
    });
    return return_txt

  }
  
  return (
    <div className="mainCalendar">
      <h1>{year}</h1>
      <div className="calendarHeader">
        <button className="button-calendar" onClick={() => changeMonth(-1)}>{"<"}</button>
        <h2>{meses[month]}</h2>
        <button className="button-calendar" onClick={() => changeMonth(1)}>{">"}</button>
      </div>
      <CalendarDays days = {days} month={month} year={year} />

    </div>
  )
}
export default CalendarMain