import React from 'react'

const CommitmentView = props => {
  let dia = window.location.href.toString().split("/")[4]
  let mes = window.location.href.toString().split("/")[5]
  let ano = window.location.href.toString().split("/")[6]
  
  let this_name = dia + "/" + mes + "/" + ano 
  return (
    <div>
      <h2>Nessa página ficaria os compromissos de {this_name}</h2>
      {props.commitments.map(element => {
        if(element.day_of === this_name){
        return (<p key={element.id}>{element.description}</p>)
        }
      })}
    </div>
  )

}

export default CommitmentView