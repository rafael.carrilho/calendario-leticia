import axios from 'axios'

const Api = axios.create({
  baseURL: "https://db-svm.herokuapp.com",
  headers: { 
    "Content-Type":"application/json",
    "Authorization":"eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoyLCJleHAiOjE2MDY2MDE1NjZ9.XFniNBmtP-WsHl8_j5a2C8nt-o-GD6BXvzjIar6uBRs"
  }
})

export default Api